# Ansible Molecule Arch Linux

[![Docker Repository on Quay](https://quay.io/repository/maartenq/ansible-molecule-archlinux/status "Docker Repository on Quay")](https://quay.io/repository/maartenq/ansible-molecule-archlinux)

Dockerfile for building container image for use for Ansible Molecule testing on Arch Linux. Supplies systemd init and ansible sudo user.
